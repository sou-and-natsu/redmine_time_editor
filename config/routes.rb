Rails.application.routes.draw do
  resources :projects do
    get  'time_editor',           :to => 'time_editor#index'
    get  'time_editor/base_info', :to => 'time_editor#base_info'
    get  'time_editor/edit',      :to => 'time_editor#edit'
    put  'time_editor/update',    :to => 'time_editor#update'
  end
end
