# TimeEditor Architecture

This documentation describe TimeEditor Architecture for reading or writing source of TimeEditor.

---
## Difference from Standard Architecture

TimeEditor Architecture has a little bit difference from standard rails architecture.

### Standard Architecture of Rails

![](images/standard_architecture_of_rails.png)

fig.1 standard architecture of rails

### TimeEditor Architecture

![](images/time_editor_architecture.png)

fig.2 TimeEditor architecture

### Different Point

* Place of View
* Language of View
* Way of communication between View and Controller

---
## Controller

TimeEditor has only one cotroller-class on server. because Controller has simple obligations such as
* Getting basic informations(for editing time_entries).
* CRUDs for time_entry.

TimeEditor's Controllers class on server is below.

![](images/controller_class_on_server.png)

fig.3 Controller Class on Server

### Disadvantage

* << support >> and << format >> methods are static. so, these would be moved to other places.

---
## Model

TimeEditor has no particular classes.

---
## View

View has a little complex structure for improving the usability.

### Structure of View

Package Structure of View is below.

![](images/package_structure_of_view.png)

fig.4 Package Structure of View

### Components of View

* app
* app.views
* app.views.gadget
* app.views.main
* app.controllers
* lib

### Controller in View

TimeEditor communicate to Server with ajax.
Controller in View is a proxy and prevent same method to Controller on Server.

![](images/controller_in_view.png)

fig.5 Controller in View

### BandView's State Transition

![](images/band_views_state_transition.png)

fig.6 BandView's State Transition

---
