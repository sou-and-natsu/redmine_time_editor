TimeEditor is a plugin to edit time visually.
like below.

![time_editor_screenshot.png](https://bitbucket.org/repo/8zXXkrX/images/2599679787-time_editor_screenshot.png)

I hope you to use TimeEditor for planning.

---

### Information

| item            | version       |
|-----------------|---------------|
| Current version | 0.0.1         |
| Compatible with | Redmine 3.3.2 |
| Browser         | Chrome ５８     |

---

### Install

1. Download redmine_time_editor-*.zip from
https://bitbucket.org/sou-and-natsu/redmine_time_editor/downloads
1. Expand the plugin into the plugins directory
1. Migrate plugin: 
   RAILS_ENV=production bundle exec rake redmine:plugins:migrate
1. Restart Redmine
1. Enable the module on the project setting page.
   Check the permissions on the Roles and permissions(Administration)

---

### Customize

you can customize TimeEditor.

#### Customize Color

Edit css file for customizing color of TimeEditor.
*redmine_time_editor/assets/stylesheets/time_editor_color.css*

exsample:
![time_editor_screenshot_colored.png](https://bitbucket.org/repo/8zXXkrX/images/73962662-time_editor_screenshot_colored.png)

---