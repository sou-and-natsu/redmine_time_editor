class PopulateCustomFields < ActiveRecord::Migration

  TIME_ENTRY_CF_START_TIME = 'start_time'

  def self.up
    if CustomField.find_by_name(TIME_ENTRY_CF_START_TIME).nil?
      TimeEntryCustomField.create(
        :name => TIME_ENTRY_CF_START_TIME,
        :description => '',
        :field_format => 'string',
        :multiple => false,
        :default_value => '00:00:00',
        :regexp => '^[0-9]{2}:[0-9]{2}:[0-9]{2}$',
        :is_required => false,
        :is_filter => false)
    end
  end

  def self.down
    unless CustomField.find_by_name(TIME_ENTRY_CF_START_TIME).nil?
      CustomField.find_by_name(TIME_ENTRY_CF_START_TIME).delete
    end
  end
end
