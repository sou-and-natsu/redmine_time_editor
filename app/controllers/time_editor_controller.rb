class TimeEditorController < ApplicationController
  unloadable

  TIME_ENTRY_CF_START_TIME = 'start_time'

  before_filter :require_login
  before_filter :find_project
  before_filter :find_cf_start_time, :only => [:edit, :update]

  # for display

  def index
  end

  # for ajax

  def base_info
    current_user = User.current
    users = User.visible
    projects = User.current.projects
    issues = Issue.visible.open
    activity = TimeEntryActivity.shared.active
    render json: {
      :current_user => filter_member_user(current_user),
      :users => users.map { |e| filter_member_user(e) },
      :projects => projects.map { |e| filter_member_project(e) },
      :issues => issues,
#      :issues => issues.map { |e| filter_member_issue(e) },
      :activities => activity.map { |e| filter_member_activity(e) }
    }
  end

  def edit
    entries = TimeEntry.visible.where(
      "user_id = ? AND spent_on >= ? AND spent_on <= ?",
      params[:user_id],
      params[:from],
      params[:to])

    entriy_ids = entries.map { |e| e.id }
    start_times = CustomValue.
      where(:custom_field_id => @cf_start_time.id).
      where(:customized_id => entriy_ids)

    entries = entries.map { |e|
      s = start_times.find { |e2| e2.customized_id == e.id }
      start_time = (s.nil?) ? ('00:00:00') : (s.value)
      {
        :id => e.id,
        :date => e.spent_on,
        :start_time => start_time,
        :end_time => calc_end_time(start_time, e.hours),
        :user_id => e.user_id,
        :project_id => e.project_id,
        :issue_id => e.issue_id,
        :activity_id => e.activity_id,
        :comments => e.comments
      }
    }

    render json: entries
  end

  def update
    insert_time_entries params[:inserts]
    update_time_entries params[:updates]
    remove_time_entries params[:removes]

    render json: []
  end

  private

  def insert_time_entries(entries)
    return if entries.nil?

    entries.each do |e|
      ActiveRecord::Base.transaction do
        entry ||= TimeEntry.new(
          user: User.find(e['user_id']),
          spent_on: Date.parse(e['date']),
          hours: to_hours(e['start_time'], e['end_time']),
          project_id: (e['issue_id'].nil?) ? (@project.id) : (nil),
          issue_id: e['issue_id'],
          activity_id: e['activity_id'],
          comments: e['comments'])
        entry.save!
        CustomValue.find_by(customized_id: entry.id).update(
          value: e['start_time'])
      end
    end
  end

  def update_time_entries(entries)
    return if entries.nil?

    entries.each do |e|
      ActiveRecord::Base.transaction do
        TimeEntry.find_by(id: e['id']).update!(
          spent_on: Date.parse(e['date']),
          hours: to_hours(e['start_time'], e['end_time']),
          project_id: (e['issue_id'].nil?) ? (@project.id) : (nil),
          issue_id: e['issue_id'],
          activity_id: e['activity_id'],
          comments: e['comments'])
        cv = CustomValue.find_by(customized_id: e['id'])
        cv.update!(value: e['start_time']) unless cv.nil?
      end
    end
  end

  def remove_time_entries(entries)
    return if entries.nil?

    entries.each do |e|
      ActiveRecord::Base.transaction do
        TimeEntry.find_by(id: e['id']).destroy!
        cv = CustomValue.find_by(customized_id: e['id'])
        cv.destroy! unless cv.nil?
      end
    end
  end

  # before

  def find_project
    @project = Project.find(params[:project_id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  def find_cf_start_time
    @cf_start_time = TimeEntryCustomField.
      where(:name => TIME_ENTRY_CF_START_TIME).to_a[0]
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  # Support

  def calc_end_time(start_time, hours)
    total = to_float_time(start_time) + hours
    hh = total.floor
    mm = ((total * 60.0).to_i % 60).floor
    ss = ((total * 3600.0).to_i % 60).floor

    hh = '%02d' % hh
    mm = '%02d' % mm
    ss = '%02d' % ss

    hh + ':' + mm + ':' + ss
  end

  def to_hours(start_time, end_time)
    to_float_time(end_time) - to_float_time(start_time)
  end

  def to_float_time(str_time)
    t = str_time.split(':')
    hh = t[0].to_f
    mm = t[1].to_f
    ss = t[2].to_f

    hh + (mm / 60.0) + (ss / 3600.0)
  end

  # Format

  def filter_member_user(e)
    {
      :id => e.id,
      :name => e.lastname + ' ' + e.firstname
    }
  end

  def filter_member_project(e)
    {
      :id => e.id,
      :name => e.name,
      :parent_id => e.parent_id
    }
  end

  def filter_member_issue(e)
    {
      :id => e.id,
      :subject => e.subject,
      :project_id => e.project_id,
      :status_id => e.status_id
    }
  end

  def filter_member_activity(e)
    {
      :id => e.id,
      :name => e.name
    }
  end

end
