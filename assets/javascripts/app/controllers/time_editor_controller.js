'user strict';

var http = {};

function TimeEditorController() {
}

TimeEditorController.prototype.baseInfo = function (callback) {
  var options = {
    type: 'GET',
    url: 'time_editor/base_info'
  };
  http.request(options, null, callback);
}

TimeEditorController.prototype.edit = function (cond, callback) {
  var options = {
    type: 'GET',
    url: 'time_editor/edit'
  };
  http.request(options, cond, callback);
}

TimeEditorController.prototype.update = function (info, callback) {
  var options = {
    type: 'PUT',
    url: 'time_editor/update'
  };
  http.request(options, info, callback);
}

//= Ajax

http.request = function (options, data, callback) {
  var data = (options.type === 'GET') ?
             ((data) ? (data) : ('')) :
             ((data) ? (JSON.stringify(data)) : (''));
  var req = $.ajax({
    async: true,
    type: options.type,
    url: options.url,
    data: data,
    contentType: 'application/json',
    dataType: 'json',
    timeout: 3000,
    success: function (response) {
      callback(null, response);
    },
    error : function(xmlHttpRequest, textStatus, errorThrown){
      callback(textStatus, null);
    }
  });

  return req;
};
