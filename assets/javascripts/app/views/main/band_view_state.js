﻿/**
 * @class BandView_State
 */
var BandView_State = {
  _onLMouseDown : function (context, e) {
  },

  _onLMouseUp : function (context, e) {
  },

  _onRMouseDown : function (context, e) {
  },

  _onRMouseUp : function (context, e) {
  },

  _onMouseMove : function (context, e) {
  },

  _onDblClick : function (context, e) {
  }

};

/**
 * @class BandView_Idle
 */
var BandView_Idle = {};
S.extend(BandView_Idle, BandView_State);
S.extend(BandView_Idle, {
  _onLMouseDown : function (context, e)
  {
    var pos = { x : e.layerX, y : e.layerY };

    var r = context._hitTest(pos);

    if      (r.res == 0) {
      context._initializeCreating(pos);
      context._changeState(BandView_Creating);
    }
    else if (r.res == 1) {
      context._initializeMovingTop(pos);
      context._changeState(BandView_MovingTop);
    }
    else if (r.res == 2) {
      context._initializeMovingBottom(pos);
      context._changeState(BandView_MovingBottom);
    }
    else if (r.res == 3) {
      context._initializeMoving(pos);
      context._changeState(BandView_Moving);
    }
  },

  _onLMouseUp : function (context, e) {
    // nop
  },

  _onMouseMove : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._changePointer(pos);
    context._changeState(BandView_Idle);
  }

});

/**
 * @class BandView_Creating
 */
var BandView_Creating = {};
S.extend(BandView_Creating, BandView_State);
S.extend(BandView_Creating, {
  _onLMouseDown : function (context, e)
  {
    // nop
  },

  _onLMouseUp : function (context, e) {
    context._finalizeCreating();
    context._changeState(BandView_Idle);
  },

  _onMouseMove : function (context, e) {
    context._moveBandEdge(e.layerY);
    context._changeState(BandView_Creating);
  }

});

/**
 * @class BandView_Moving
 */
var BandView_Moving = {};
S.extend(BandView_Moving, BandView_State);
S.extend(BandView_Moving, {
  _onLMouseDown : function (context, e) {
    // nop
  },

  _onLMouseUp : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._finalizeMoving(pos);
    context._changeState(BandView_Idle);
  },

  _onMouseMove : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._moveSelecting(pos);
    context._changeState(BandView_Moving);
  }

});

/**
 * @class BandView_MovingTop
 */
var BandView_MovingTop = {};
S.extend(BandView_MovingTop, BandView_State);
S.extend(BandView_MovingTop, {
  _onLMouseDown : function (context, e) {
    // nop
  },

  _onLMouseUp : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._finalizeMovingTop(pos);
    context._changeState(BandView_Idle);
  },

  _onMouseMove : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._moveSelectingTop(pos);
    context._changeState(BandView_MovingTop);
  }

});

/**
 * @class BandView_MovingBottom
 */
var BandView_MovingBottom = {};
S.extend(BandView_MovingBottom, BandView_State);
S.extend(BandView_MovingBottom, {
  _onLMouseDown : function (context, e) {
  },

  _onLMouseUp : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._finalizeMovingBottom(pos);
    context._changeState(BandView_Idle);
  },

  _onMouseMove : function (context, e) {
    var pos = { x : e.layerX, y : e.layerY };

    context._moveSelectingBottom(pos);
    context._changeState(BandView_MovingBottom);
  }
});
