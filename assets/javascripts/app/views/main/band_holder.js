﻿function calcTotalLength(bands)
{
  var total = 0;

  for (var i = 0; i < bands.length; ++i)
  {
    total += bands[i].height();
  }

  return total;
}

/**
 * @class BandHolder
 */

function BandHolder(handler, width) {
  this._handler = handler;

  this._bands = new Array();
  this._width = width;
}

// = Operation

BandHolder.prototype.add = function (band)
{
  var column = this._getColumnIndex(band);

  var bands = this._getBands(column);

  bands.push(band);

  this._handler.notifyChange(column, calcTotalLength(bands));
};

BandHolder.prototype.remove = function (band)
{
  for (var i = 0; i < this._bands.length; i++)
  {
    if (this._bands[i] == band)
    {
      delete this._bands[i];

      this._bands.splice(i, 1);

      break;
    }
  }
  var column = this._getColumnIndex(band);
  this._handler.notifyChange(column, calcTotalLength(this._bands[column]));
};

BandHolder.prototype.add_unit = function (band)
{
  var column = this._getColumnIndex(band);

  var bands = this._getBands(column);

  bands.push(band);

  this._handler.notifyChange(column, calcTotalLength(bands));
};

BandHolder.prototype.remove_unit = function (band)
{
  var column = this._getColumnIndex(band);

  //var bands = this._getBands(column);

  for (var j = 0; j < this._bands[column].length; j++)
  {
      if (this._bands[column][j] == band)
      {
        //delete bands[j];

        this._bands[column].splice(j, 1);

        break;
      }
  }
  this._handler.notifyChange(column, calcTotalLength(this._bands[column]));
};

BandHolder.prototype.clear = function ()
{
  var lenght = this._bands.length;
  for (var i = (lenght - 1); i >= 0; i-- )
  {
    var bands = this._bands[i];
    if (bands != null)
    {
      for (var j = (bands.length - 1); j >= 0; j--)
      {
        this._bands[i][j].finalize();

        delete this._bands[i][j];
      }
    }

    delete this._bands[i];
  }

  this._bands = new Array();

  this._handler.notifyChange();
};

BandHolder.prototype.hitTest = function (pos)
{
  var result =
  {
    res    : 0,
    hit    : null,
    column : this._getColumnIndexByX(pos.x)
    //column : this._getColumnIndexByXRoundZero(pos.x+50)
  };

var bands = this._getBands(result.column);

  for (var j = 0; j < bands.length; j++)
  {
    var band = bands[j];

    var res = band.hitTest(pos);
    if (res != 0)
    {
      result.res = res;
      result.hit = band;

      break;
    }
  }

  return result;
};

// =
// = Support

BandHolder.prototype._getBands = function (column)
{
  if (this._bands[column] == null)
  {
    this._bands[column] = new Array();
  }

  return this._bands[column];
};

BandHolder.prototype._getColumnIndex = function (band)
{
  return this._getColumnIndexByX(band.left());
};

BandHolder.prototype._getColumnIndexByX = function (y)
{
  //return Math.ceil(y / this._width);
  return Math.round(y / this._width);
};
