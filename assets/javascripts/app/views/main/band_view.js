//------------------------------------------------------------------------------
// WorkInfo

function WorkInfo(date, start_time, end_time) {
  this.date       = date;
  this.start_time = start_time;
  this.end_time   = end_time;

  this.wf_no        = "";
  this.issue_id      = -1;
  this.cr_id        = "";

  this.work_kind_id = -1;
  this.comments      = "";
};

WorkInfo.prototype.getString = function () {
  return this.start_time;
}

/**
 * @class BandView
*/

function BandView(parent, top, left, handler) {
  this._state = BandView_Idle;

  this._band_holder = new BandHolder(this, this.WORK_COLUMN_WIDTH);

  this._base       = this._createBase      (parent, top, left);
  this._header     = this._createHeader    (this._base, 0, 50);
  this._background = this._createBackground(this._base, 50, 0);
  this._foreground = this._createForeground(this._background);
  this._footer     = this._createFooter    (this._base, 530, 50);

  this._selecting  = null;

  this._work_kind = null;
  this._issue    = null;

  this._works   = new Array();
  this._handler = handler;

  this.invalidate();

  this._background.scrollTop = 320;
}

// =

/*
BandView.prototype.DAY_OF_THE_WEEK =
[
  '日', '月', '火', '水', '木', '金', '土'
],
*/

BandView.prototype.DAY_OF_THE_WEEK =
[
  'So', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'
],

// = Base Attribute

BandView.prototype.WIDTH = function ()
{
  return (this.TIME_COLUMN_WIDTH + (this.WORK_COLUMN_WIDTH * this.WORK_COLUMN_NUM) + this.SCROLL_BAR_WIDTH);
};

BandView.prototype.SCROLL_BAR_WIDTH  = 17;

BandView.prototype.TIME_COLUMN_WIDTH = 50;

BandView.prototype.WORK_COLUMN_WIDTH = 100;

BandView.prototype.WORK_COLUMN_NUM   = 7;

BandView.prototype.QUARTER_HEIGHT    = 10;

// = Band Attribute

BandView.prototype.SETUPED_DEFAULT_COLOR       = "palegreen";

BandView.prototype.SETUPED_SELECTING_COLOR     = "turquoise";

BandView.prototype.UNSETUPED_DEFAULT_COLOR     = "indianred";

BandView.prototype.UNSETUPED_SELECTING_COLOR   = "lightcoral";

BandView.prototype.DEVELOPMENT_DEFAULT_COLOR   = "blueviolet";

BandView.prototype.DEVELOPMENT_SELECTING_COLOR = "mediumslateblue";

BandView.prototype.STUDY_DEFAULT_COLOR         = "#d7c447";

BandView.prototype.STUDYNAT_SELECTING_COLOR    = "#9caeb7";

BandView.prototype.NAT_DEFAULT_COLOR           = "dodgerblue";

BandView.prototype.NAT_SELECTING_COLOR         = "skyblue";

BandView.prototype.BAND_STYLE = {
  zIndex      :  70,

  borderStyle : "solid",
  borderWidth : "1px",
  borderColor : "green",

  overflow    : "hidden",

  opacity     : 0.6
};

// =

BandView.prototype.baseInfo = function (info) {
  this._issue = info.issues;
  this._work_kind = info.work_kinds;
};

BandView.prototype.validate = function () {
  this._validateForground();
};

BandView.prototype.invalidate = function () {
  this._invalidateForground();
};

// =

BandView.prototype.setStartDate = function (date) {
  this._setStartDate(date);
};

BandView.prototype.setData = function (works) {
  this.clear();

  this._works = works;

  var total_time = 0;

  this._OffTimeInit();

  array = new Array(this.WORK_COLUMN_NUM);

  for (var i = 0; i <  this.WORK_COLUMN_NUM; i++) {
    array[i] = 0;
  }

  for (var i = 0; i < works.length; i++) {
    var band = this._createBand(works[i]);
    if (band == null) {
      continue;
    }

    if ((450 == works[i].work_kind_id)||
        (500 == works[i].work_kind_id)) {
       var date = works[i].date;
       date = date.replace(/-/g,"\/");

       var date_ed = date + " " + works[i].end_time;
       var date_st = date + " " + works[i].start_time;

       dd_ed = new Date(date_ed);
       dd_st = new Date(date_st);

       var time = dd_ed.getTime() - dd_st.getTime();

       var column =this._dateTocolumn(works[i].date);

       array[column] += time;
    }

    this._band_holder.add(band);
  }

  for (var i = 0; i <  this.WORK_COLUMN_NUM; i++) {
    if (0 != array[i]) {
      dd_total = new Date();
      dd_total.setTime(array[i]-32400000);

      dd_hour = new Date();
      dd_hour = dd_total.getHours();
      dd_time = new Date();
      dd_time = dd_total.getMinutes();

      this._footer.setText(1, i, dd_hour.toString()+":"+dd_time.toString() );
    }
  }
};

BandView.prototype.getSelectingInfo = function () {
  var selecting = this._getSelecting();
  if (selecting == null) {
    return null;
  }

  return selecting._data;
};

BandView.prototype.setSelectingInfo = function (info) {
  var band = this._getSelecting();
  if (band == null) {
    return null;
  }

  var original_info = band._data;

  if (isSameWorkInfoAttr(original_info, info) == false) {
    info.updated = 1;
    S.extend(original_info, info);

    this._setSelectingColor(band);
  }
};

BandView.prototype.clear = function () {
  this._setSelecting();
  this._band_holder.clear();
};

BandView.prototype._OffTimeInit = function () {

  for (var i = 0; i <  this.WORK_COLUMN_NUM; i++) {
    this._footer.setText(1, i, "" );
  }
};

BandView.prototype._createBand = function (work) {
  var top    = this._timeToY(work.start_time);
  var bottom = this._timeToY(work.end_time);
  var height = bottom - top;
  var left   = this._dateToX(work.date);

  if ((left < 0) || (left >= 700)) {
    return null;
  }

  var rect = {
    top    : top,
    left   : left,
    height : height,
    width  : 100
  };

  var div = new Div(this._background, rect, this.BAND_STYLE);
  div._data = work;

  var text = '';
  if (work.issue_id != '-1') {
    text = '#' + work.issue_id;
  } else if (work.comments != '') {
    text = work.comments;
  }
  div._element.innerHTML = text;

  this._setDefaultColor(div);

  return div;
};

// =

BandView.prototype._timeToY = function (time_string) {
  var time = time_string.split(":");
  var hour = eval(time[0]);
  var min  = eval(time[1]);

  var y = (hour * 40) + ((min / 15) * 10);

  return y;
};

BandView.prototype._yToTime = function (y) {
  var hour = Math.ceil((y - 39) / 40);
  var min  = Math.ceil((y % 40) / 40 * 60);

  min = (min == 0) ? ("00") : (min);

  var time_string = "" + hour + ":" + min + ":00";

  return time_string;
};

BandView.prototype._dateToX = function (date_string) {
  var start_date_time = this._start_date.getTime();

  var d     = date_string.split("-");
  var year  = eval(d[0]);
  var month = eval(d[1]) - 1;
  var date  = eval(d[2]);
  var date_time = new Date(year, month, date).getTime();

  var column = Math.ceil((date_time - start_date_time) / (24 * 3600 * 1000));

  return (50 + (column * 100));
};

BandView.prototype._dateTocolumn = function (date_string) {
  var start_date_time = this._start_date.getTime();

  var d     = date_string.split("-");
  var year  = eval(d[0]);
  var month = eval(d[1]) - 1;
  var date  = eval(d[2]);
  var date_time = new Date(year, month, date).getTime();

  var column = Math.ceil((date_time - start_date_time) / (24 * 3600 * 1000));

  return column;
};

BandView.prototype._xToDate = function (x) {
  var diff = Math.floor(x / 100);

  var date  = DateUtil.nextDate(this._start_date, diff);

  var year  = date.getFullYear();
  var month = date.getMonth() + 1;
  var day   = date.getDate();

  var date_string = "" + year + "-" + month + "-" + day;

  return date_string;
};

// = Header Operation

BandView.prototype._setStartDate = function (date) {
  this._start_date = date;

  for (var i = 0; i < this.WORK_COLUMN_NUM; i++)
  {
    this._header.setText(0, i, this.DAY_OF_THE_WEEK[date.getDay()]);
    this._header.setText(1, i, "" + (date.getMonth() + 1) + "/" + date.getDate());

    date = DateUtil.nextDate(date);
  }
};

// = private UI

BandView.prototype._createBase = function (parent, top, left) {
  var e = document.createElement('div');
  e.classList.add('band-view');

  e.style.position = "absolute";
  e.style.top      = "" + top  + "px";
  e.style.left     = "" + left + "px";

  e.style.overflow = "visible";

  parent.appendChild(e);

  return e;
};

BandView.prototype._createHeader = function (parent, top, left) {
  var TABLE_ATTR = {
    CELL_HEIGHT :  24,
    CELL_WIDTH  :  98,

    setupTable : function (table, row_num, column_num) {
      table.style.width  = "" + ((this.CELL_WIDTH + 2) * column_num) + "px";
      table.cellPadding  = 0;
      table.cellSpaceing = 0;
    },

    setupRow : function (row, row_index) {
      row.style.height = "" + this.CELL_HEIGHT + "px";
    },

    setupCell : function (cell, row_index, column_index) {
      cell.classList.add(((row_index % 2) == 0) ?
                         ('column-header-1') :
                         ('column-header-2'));
      cell.style.width  = "" + this.CELL_WIDTH  + "px";
      cell.style.textAlign = "center";
      cell.style.padding = 0;
    }
  };

  var e = new SimpleTable(parent, top, left, TABLE_ATTR);
  e.build(2, this.WORK_COLUMN_NUM);

  return e;
};

BandView.prototype._createBackground = function (parent, top, left) {
  var e = document.createElement('div');
  e.classList.add('body-background');

  e.style.position = "absolute";
  e.style.top      = "" + top  + "px";
  e.style.left     = "" + left + "px";
  e.style.height   = "480px";
  e.style.width    = "" + this.WIDTH() + "px";

  e.style.overflowY = "scroll";
  e.style.overflowX = "hidden";

  parent.appendChild(e);

  this._createTimeColumn(e);
  this._createWorkColumn(e);
  this._createHorizontalLine(e);
//  this._createLunchTimeLine(e);
//  this._createBreakTimeLine(e);
//  this._createDinerTimeLine(e);

  return e;
};

BandView.prototype._createTimeColumn = function (parent) {
  for (var i = 0; i < 24; i++) {
    var rect = {
      top    : (i * 40),
      left   : 0,
      height : 40,
      width  : 50
    };
    var style = {
      textAlign: "right"
    };
    var e = new Div(parent, rect, style);
    e._element.classList.add(((i % 2) == 0) ? ('row-header-1') : ('row-header-2'));

    e.setText("" + i + ":00");
  }
};

BandView.prototype._createWorkColumn = function (parent) {
  for (var i = 0; i < this.WORK_COLUMN_NUM; i++) {
    var e = document.createElement('div');
    e.classList.add(((i % 2) == 0) ? ('body-column-1') : ('body-column-2'));

    e.style.position = "absolute";
    e.style.top      = "0px";
    e.style.left     = "" + (50 + (100 * i)) + "px";
    e.style.height   = "960px";
    e.style.width    = "100px";

    e.style.zIndex = 10;

    parent.appendChild(e);
  }
};

BandView.prototype._createHorizontalLine = function (parent) {
  for (var i = 0; i < 48; i++) {
    var e = document.createElement('div');
    e.classList.add('body-horizontal-line');

    e.style.position = "absolute";
    e.style.top      = "" + ((i * 20) - 1) + "px";
    e.style.left     = "0px";
    e.style.height   = "0px";
    e.style.width    = "" + this.WIDTH() + "px";

    e.style.borderStyle = ((i % 2) == 0) ? ("solid") : ("dotted");
    e.style.borderWidth = "1px";

    e.style.zIndex = 20;

    parent.appendChild(e);
  }
};

BandView.prototype._createLunchTimeLine = function (parent) {
  var e = document.createElement('div');

  e.style.position = "absolute";
  e.style.top      = "" + (((24 * 20) + 10) - 1) + "px";
  e.style.left     = "" + (50) + "px";;
  e.style.height   = "0px";
  e.style.width    = "" + this.WIDTH() + "px";

  e.style.borderStyle = ("solid");
  e.style.borderWidth = "15px";
  e.style.borderColor = "lightgrey";

  e.style.zIndex = 20;

  parent.appendChild(e);
};

BandView.prototype._createBreakTimeLine = function (parent) {
  var e = document.createElement('div');

  e.style.position = "absolute";
  e.style.top      = "" + ((34 * 20) - 1) + "px";
  e.style.left     = "" + (50) + "px";;
  e.style.height   = "0px";
  e.style.width    = "" + this.WIDTH() + "px";

  e.style.borderStyle = ("solid");
  e.style.borderWidth = "5px";
  e.style.borderColor = "lightgrey";

  e.style.zIndex = 20;

  parent.appendChild(e);
};

BandView.prototype._createDinerTimeLine = function (parent) {
  var e = document.createElement('div');

  e.style.position = "absolute";
  e.style.top      = "" + ((42 * 20) + 30 - 1) + "px";
  e.style.left     = "" + (50) + "px";;
  e.style.height   = "0px";
  e.style.width    = "" + this.WIDTH() + "px";

  e.style.borderStyle = ("solid");
  e.style.borderWidth = "10px";
  e.style.borderColor = "lightgrey";

  e.style.zIndex = 20;

  parent.appendChild(e);
};

BandView.prototype._createForeground = function (parent) {
  var e = document.createElement('div');

  e.style.position = "absolute";
  e.style.top      = "0px";
  e.style.left     = "0px";
  e.style.height   = "960px";
  e.style.width    = "" + (50 + (this.WORK_COLUMN_NUM * 100)) + "px";

  e.style.zIndex   = 120;

  e.style.opacity  = 0.1;

  parent.appendChild(e);

  return e;
};

BandView.prototype._createFooter = function (parent, top, left) {
  var TABLE_ATTR = {
    CELL_HEIGHT :  24,
    CELL_WIDTH  :  98,

    setupTable : function (table, row_num, column_num) {
      table.style.width  = "" + ((this.CELL_WIDTH + 2) * column_num) + "px";
      table.cellPadding  = 0;
      table.cellSpaceing = 0;
    },
    setupRow : function (row, row_index) {
      row.style.height = "" + this.CELL_HEIGHT + "px";
    },
    setupCell : function (cell, row_index, column_index) {
      cell.classList.add(((row_index % 2) == 0) ?
                         ('column-header-1') :
                         ('column-header-2'));
      cell.style.width  = "" + this.CELL_WIDTH  + "px";
      cell.style.textAlign = "center";
      cell.style.padding = 0;
    }
  };
  var e = new SimpleTable(parent, top, left, TABLE_ATTR);
  e.build(2, this.WORK_COLUMN_NUM);

  return e;
};

// =

BandView.prototype._validateForground = function () {
  var e = this._foreground;

  e.style.backgroundColor = "";

  e.addEventListener("mousedown", this._onMouseDown.bind(this), true);
  e.addEventListener("mouseup",   this._onMouseUp.bind(this),   true);
  e.addEventListener("mousemove", this._onMouseMove.bind(this), true);
  e.addEventListener("dblclick",  this._onDblClick.bind(this),  true);
};

BandView.prototype._invalidateForground = function () {
  var e = this._foreground;

  e.style.backgroundColor = "limegreen";

  e.removeEventListener("mousedown", this._onMouseDown.bind(this), true);
  e.removeEventListener("mouseup",   this._onMouseUp.bind(this),   true);
  e.removeEventListener("mousemove", this._onMouseMove.bind(this), true);
  e.removeEventListener("dblclick",  this._onDblClick.bind(this),  true);
};

// =

// = State Manage

BandView.prototype._changeState = function (state) {
  this._state = state;
};

// = Support

BandView.prototype._calcPosition = function (pos) {
  var range = {
    top    : (parseInt((pos / this.QUARTER_HEIGHT) + 0) * this.QUARTER_HEIGHT),
    bottom : (parseInt((pos / this.QUARTER_HEIGHT) + 1) * this.QUARTER_HEIGHT)
  };

  return range;
};

BandView.prototype._calcColumnIndex = function (x) {
  return parseInt((x - 50) / 100);
};

// = Selecting Operation

BandView.prototype._setSelecting = function (div) {
  if ((div             != null) &&
      (this._selecting != null) &&
      (this._selecting == div)) {
    return;
  }

  if (this._selecting != null) {
    this._setDefaultColor(this._selecting);

    this._handler.onBlurSelecting();
  }

  this._selecting = div;

  if (this._selecting != null) {
    this._setSelectingColor(this._selecting);

    this._handler.onFocusSelecting(this._selecting._data);
  }
};

BandView.prototype._setDefaultColor = function (div) {
  if (!div) {
    return;
  }

  var color = this._getDefaultColor(div);

  div.setColor(color);
};

BandView.prototype._setSelectingColor = function (div) {
  if (!div) {
    return;
  }

  var color = this._getSelectingColor(div);

  div.setColor(color);
};

BandView.prototype._getDefaultColor = function (div) {
  return (this._isSetuped(div)) ?
         (this.SETUPED_DEFAULT_COLOR) :
         (this.UNSETUPED_DEFAULT_COLOR);
};

BandView.prototype._getSelectingColor = function (div) {
  return (this._isSetuped(div)) ?
         (this.SETUPED_SELECTING_COLOR) :
         (this.UNSETUPED_SELECTING_COLOR);
};

BandView.prototype._isSetuped = function (div) {
  if (!div) {
    return false;
  }

  if (!div._data || div._data.issue_id == -1) {
    return false;
  }

  return true;
};

BandView.prototype._getSelecting = function () {
  return this._selecting;
};

BandView.prototype.removeSelecting = function () {
  var selecting = this._getSelecting();
  if (selecting == null) {
    return;
  }

  this._band_holder.remove_unit(selecting);

  this._removeData(selecting);

  this._setSelecting();

  selecting.finalize();
};

BandView.prototype.moveSelectingUp = function () {
  var selecting = this._getSelecting();
  if (selecting == null) {
    return;
  }

  selecting.moveBy(0, -(this.QUARTER_HEIGHT));
};

BandView.prototype.moveSelectingDown = function () {
  var selecting = this._getSelecting();
  if (selecting == null) {
    return;
  }

  selecting.moveBy(0, this.QUARTER_HEIGHT);
};

BandView.prototype.selectNext = function () {
  // TODO
};

BandView.prototype.selectPrev = function () {
  // TODO
};

// = Data Operation

BandView.prototype._updateData = function (band) {
  var original_info = band._data;
  var info =
  {
    date       : this._xToDate(band.left()),
    start_time : this._yToTime(band.top()),
    end_time   : this._yToTime(band.bottom())
  }

  if (isSameWorkInfoTime(original_info, info) == false) {
    info.updated = 1;
    S.extend(original_info, info);
  }
};

BandView.prototype._removeData = function (band) {
  band._data.removed = 1;
};

// = Event Handler from band_holder

BandView.prototype.notifyChange = function  (column, total_height) {
  if (column == null) {
    // clear
    for (var i = 0; i < this.WORK_COLUMN_NUM; ++i) {
      this._footer.setText(0, i,  "");
    }

    return;
  }

  var hour = Math.floor(total_height / 40);
  var min  = Math.floor((total_height % 40) / 40 * 60);

  min = (min == 0) ? ("00") : (min);

  var time_string = "" + hour + ":" + min;

  if ("0:00" == time_string) {
      time_string = "";
  }

  this._footer.setText(0, (column - 1),  time_string);
};

// = Event Handler from User

BandView.prototype._onMouseDown = function (e) {
  if      (e.button == 0) {
    this._onLMouseDown(e);
  }
  else if (e.button == 2) {
    this._onRMouseDown(e);
  }
};

BandView.prototype._onMouseUp = function (e)
{
  if      (e.button == 0) {
    this._onLMouseUp(e);
  }
  else if (e.button == 2) {
    this._onRMouseUp(e);
  }
};

BandView.prototype._onLMouseDown = function (e) {
  this._state._onLMouseDown(this, e);
};

BandView.prototype._onLMouseUp = function (e) {
  this._state._onLMouseUp(this, e);
};

BandView.prototype._onRMouseDown = function (e) {
  this._state._onRMouseDown(this, e);
};

BandView.prototype._onRMouseUp = function (e) {
  this._state._onRMouseUp(this, e);
};

BandView.prototype._onMouseMove = function (e) {
  this._state._onMouseMove(this, e);
};

BandView.prototype._onDblClick = function (e) {
  this._state._onDblClick(this, e);
};

// = Common Opearation

BandView.prototype._hitTest = function (pos) {
  return this._band_holder.hitTest(pos);
};

// = In Idle

BandView.prototype._changePointer = function (pos) {
  var r = this._hitTest(pos);
  var cursor = "auto";
  if      (r.res == 1) {
    cursor = "n-resize";
  }
  else if (r.res == 2) {
    cursor = "s-resize";
  }
  else if (r.res == 3) {
    cursor = "move";
  }

  this._background.style.cursor = cursor;
};

// = In Creating

BandView.prototype._initializeCreating = function (pos) {
  var range  = this._calcPosition(pos.y);
  var column = this._calcColumnIndex(pos.x);

  var rect = {
    top    : range.top,
    left   : (50 + (column * 100)),
    height : this.QUARTER_HEIGHT,
    width  : 100
  };
  var div = new Div(this._background, rect, this.BAND_STYLE);
  div._data = new WorkInfo();

  this._setSelecting(div);

  this._original_range  = range;
  this._original_colomn = column;
};

BandView.prototype._moveBandEdge = function (pos) {
  var range = this._calcPosition(pos);

  var top    = Math.min(this._original_range.top,    range.top);
  var bottom = Math.max(this._original_range.bottom, range.bottom);

  this._selecting.moveTopTo(top);
  this._selecting.moveBottomTo(bottom);
};

BandView.prototype._finalizeCreating = function () {
  this._updateData(this._selecting);

  this._works.push(this._selecting._data);
  this._band_holder.add(this._selecting);

  this._original_range  = null;
  this._original_colomn = -1;
};

// = In Moving

BandView.prototype._initializeMoving = function (pos) {
  var r = this._hitTest(pos);
  this._setSelecting(r.hit);

  this._original_position = pos;

  this._last_diff_x = 0;
  this._last_diff_y = 0;

  this._min_diff_x  =   0 - this._selecting.left();
  this._max_diff_x  = 750 - this._selecting.right();
  this._min_diff_y  =   0 - this._selecting.top();
  this._max_diff_y  = 960 - this._selecting.bottom();

  this._deleteSelecting(pos);
};

BandView.prototype._deleteSelecting = function (pos) {
  this._band_holder.remove_unit(this._selecting);

  var column = this._band_holder._getColumnIndexByX(pos.x);
  var bands = this._band_holder._getBands(column);
  this._band_holder._handler.notifyChange(column, calcTotalLength(bands));
};

BandView.prototype._moveSelecting = function (pos) {
  var diff_x = Math.ceil((pos.x - this._original_position.x) / 10) * 10;
  var diff_y = Math.ceil((pos.y - this._original_position.y) / 10) * 10;

  diff_x = Math.min(diff_x, this._max_diff_x);
  diff_x = Math.max(diff_x, this._min_diff_x);

  diff_x = (diff_x >= 0) ?
           (Math.floor(diff_x / this.WORK_COLUMN_WIDTH) * this.WORK_COLUMN_WIDTH) :
           (Math.ceil (diff_x / this.WORK_COLUMN_WIDTH) * this.WORK_COLUMN_WIDTH);

  diff_y = Math.min(diff_y, this._max_diff_y);
  diff_y = Math.max(diff_y, this._min_diff_y);

  var move_x = diff_x - this._last_diff_x
  var move_y = diff_y - this._last_diff_y;

  this._selecting.moveBy(move_x, move_y);

  this._last_diff_x = diff_x;
  this._last_diff_y = diff_y;
};

BandView.prototype._finalizeMoving = function (pos) {
  this._updateData(this._selecting);

  this._original_position = null;

  this._last_diff_x = null;
  this._last_diff_y = null;

  this._min_diff_x  = null;
  this._max_diff_x  = null;
  this._min_diff_y  = null;
  this._max_diff_y  = null;

  this._upDateSelecting(pos);
};

BandView.prototype._upDateSelecting = function (pos) {
  this._band_holder.add_unit(this._selecting);

  var column = this._band_holder._getColumnIndexByX(pos.x);
  var bands = this._band_holder._getBands(column);
  this._band_holder._handler.notifyChange(column, calcTotalLength(bands));
};

// = In Moving Top

BandView.prototype._initializeMovingTop = function (pos) {
  var r = this._hitTest(pos);
  this._setSelecting(r.hit);

  this._original_position = pos;
  this._last_diff_y       = 0;
  this._min_diff_y        = 0 - this._selecting.top();
  this._max_diff_y        = this._selecting.height() - 10;
};

BandView.prototype._moveSelectingTop = function (pos) {
  var diff_y = Math.ceil((pos.y - this._original_position.y) / 10) * 10;
  diff_y = Math.min(diff_y, this._max_diff_y);
  diff_y = Math.max(diff_y, this._min_diff_y);

  var move_y = diff_y - this._last_diff_y;
  this._selecting.moveTopBy(move_y);

  this._last_diff_y = diff_y;
};

BandView.prototype._finalizeMovingTop = function (pos) {
  this._updateData(this._selecting);

  this._original_position = null;
  this._last_diff_y       = null;
  this._min_diff_y        = null;
  this._max_diff_y        = null;
};

// = In Moving Bottom

BandView.prototype._initializeMovingBottom = function (pos) {
  var r = this._hitTest(pos);
  this._setSelecting(r.hit);

  this._original_position = pos;
  this._last_diff_y       = 0;
  this._min_diff_y        =  10 - this._selecting.height();
  this._max_diff_y        = 960 - this._selecting.bottom();
};

BandView.prototype._moveSelectingBottom = function (pos) {
  var diff_y = Math.ceil((pos.y - this._original_position.y) / 10) * 10;
  diff_y = Math.min(diff_y, this._max_diff_y);
  diff_y = Math.max(diff_y, this._min_diff_y);

  var move_y = diff_y - this._last_diff_y;
  this._selecting.moveBottomBy(move_y);

  this._last_diff_y = diff_y;
};

BandView.prototype._finalizeMovingBottom = function (pos) {
  this._updateData(this._selecting);

  this._original_position = null;
  this._last_diff_y       = null;
  this._min_diff_y        = null;
  this._max_diff_y        = null;
};

// = support

function isSameWorkInfoTime(lhs, rhs) {
  if ( lhs.date       != rhs.date )         { return false; };
  if ( lhs.start_time != rhs.start_time )   { return false; };
  if ( lhs.end_time   != rhs.end_time )     { return false; };

  return true;
}

function isSameWorkInfoAttr(lhs, rhs) {
  if (lhs.issue_id    != rhs.issue_id)    { return false; };
  if (lhs.activity_id != rhs.activity_id) { return false; };
  if (lhs.comments    != rhs.comments)    { return false; };

  return true;
}
