/**
 * @class InfoPanel
 */

function InfoPanel(panent, top, left, width, handler) {
  this._base =
  this._createBase(panent, top, left, width);

  this._createElements(this._base);

  this._handler = handler;
}

InfoPanel.prototype.HEIGHT = 180;

// = public

InfoPanel.prototype.baseInfo = function (info) {
  this._project.options(info.projects);
  this._issues = info.issues;
  this._activity.options(info.activities);
};

// =

InfoPanel.prototype.setInfo = function (info) {
  if (info == null) {
    this._project.value  (-1);
    this._issue.value    (-1);
    this._activity.value (-1);
    this._comments.value ("");

    this._project.enable  (false);
    this._issue.enable    (false);
    this._activity.enable (false);
    this._comments.enable (false);

    return;
  }

  var issue_id = info.issue_id;
  var project_id = this._getProjectId(issue_id);
  var issues = this._collectIssuesWith(project_id, issue_id);

  this._issue.options(issues);

  this._project.value  (project_id);
  this._issue.value    (issue_id);
  this._activity.value (info.activity_id);
  this._comments.value (info.comments);

  this._project.enable  (true);
  this._issue.enable    (project_id != -1);
  this._activity.enable (true);
  this._comments.enable (true);
};

InfoPanel.prototype.getInfo = function () {
  return {
    project_id: this._project.value(),
    issue_id: this._issue.value(),
    activity_id: this._activity.value(),
    comments: this._comments.value()
  };
};

// =

InfoPanel.prototype._getProjectId = function (issue_id) {
  var issue = this._issues.find(function (e) {
    return e.id == issue_id;
  });

  return (issue) ? (issue.project_id) : ('-1');
};

// =

InfoPanel.prototype._collectIssues = function (project_id) {
  return this._issues.filter(function (e) {
    return e.project_id == project_id;
  });
};

InfoPanel.prototype._collectIssuesWith = function (project_id, issue_id) {
  var issues = [];

  if (-1 == project_id) {
    return issues;
  }

  var length = this._issues.length;
  for (var i = 0; i < length; i++) {
    var issue = this._issues[i];

    if (issue.issue_id == issue_id) {
      issues.unshift(issue);
    } else if (issue.project_id == project_id) {
      issues.push(issue);
    }
  }

  return issues;
};

// = UI

InfoPanel.prototype._createBase = function (panent, top, left, width) {
  var e = document.createElement('div');
  e.classList.add('info-panel');

  e.style.position = "absolute";
  e.style.top      = "" + top    + "px";
  e.style.left     = "" + left   + "px";
  e.style.height   = "" + this.HEIGHT + "px";
  e.style.width    = "" + width  + "px";

  e.style.overflow        = "hidden";

  e.style.borderStyle = "solid";
  e.style.borderWidth = "1px";
  e.style.borderColor = "black";

  panent.appendChild(e);

  return e;
};

InfoPanel.prototype._createElements = function (parent) {
  new Text       (parent,  10,   5, 100, 'Project');
  var PROJECT_SELECT_LENDERER = {
    label : function (item) { return item.name; },
    value : function (item) { return item.id;   }
  };
  this._project =
  new Select     (parent,  10,  60, 104, PROJECT_SELECT_LENDERER, true);
  new Text       (parent,  40,   5, 100, 'Issue');
  var ISSUE_SELECT_LENDERER = {
    label : function (item) { return item.subject;  },
    value : function (item) { return item.id; }
  };
  this._issue =
  new Select     (parent,  40,  60, 104, ISSUE_SELECT_LENDERER, true);
  new Text       (parent,  70,   5, 100, 'Activity');
  var ACTIVITY_SELECT_LENDERER = {
    label : function (item) { return item.name; },
    value : function (item) { return item.id;   }
  };
  this._activity =
  new Select     (parent,  70,  60, 104, ACTIVITY_SELECT_LENDERER, false);

  new HLine      (parent, 100,   5, 165);

  new Text       (parent, 110,   5, 100, 'Comment');
  this._comments =
  new TextArea   (parent, 130,   4, 36, 154);

  // Event Bind
  this._project.setOnChange(this._onProjectChanged.bind(this));
  this._issue.setOnChange(this._onIssueChanged.bind(this));
  this._activity.setOnChange(this._onActivityChanged.bind(this));
};

// = Event

InfoPanel.prototype._onProjectChanged = function () {
  var project_id = this._project.value();
  var issues = this._collectIssues(project_id);

  this._issue.options(issues);
  this._issue.value(-1);
  this._issue.enable((0 < issues.length));

  this._fireWorkInfoChanged();
};

InfoPanel.prototype._onIssueChanged = function () {
  this._fireWorkInfoChanged();
};

InfoPanel.prototype._onActivityChanged = function () {
  this._fireWorkInfoChanged();
};

// = fire

InfoPanel.prototype._fireWorkInfoChanged = function () {
  var info = this.getInfo();
  this._handler.onWorkInfoChanged(info);
};
