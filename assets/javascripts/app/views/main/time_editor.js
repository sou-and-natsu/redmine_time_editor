// = Model

function createUpdateWorkInfo(works, user_id) {

  function toUnsignedInt(e) {
    return (e == -1) ? (null) : (parseInt(e));
  }

  function toTimeEntry(e) {
    e.user_id     = toUnsignedInt(e.user_id);
    e.project_id  = toUnsignedInt(e.project_id);
    e.issue_id    = toUnsignedInt(e.issue_id);
    e.activity_id = toUnsignedInt(e.activity_id);

    return e;
  }

  var inserts = [];
  var updates = [];
  var removes = [];

  for (var i = 0; i < works.length; i++) {
    var work = works[i];

    work.user_id = user_id;

    if      ((work['id'] == null) &&
             (work['removed'] == null))
    {
      delete work.updated;

      inserts.push(work);
    }
    else if ((work['id'] != null) &&
             (work['removed'] != null))
    {
      delete work.removed;

      removes.push({ id : work.id });
    }
    else if ((work['id'] != null) &&
             (work['updated'] != null))
    {
      delete work.updated;

      updates.push(work);
    }
  }

  return {
    inserts : inserts.map(toTimeEntry),
    updates : updates.map(toTimeEntry),
    removes : removes.map(toTimeEntry)
  };
}

/**
 * TimeEditor
 */

function TimeEditor() {
}

TimeEditor.prototype.OFFSET_DATE = 7;

TimeEditor.prototype.MESSAGE_CONFIRM_DELETE_SELECTING = "Are you sure you want to delete?";

// =

TimeEditor.prototype.load = function (parent) {
  this._controller = new TimeEditorController();
  this._base_info = [];
  this._work_info = [];

  this._createElements(parent);

  window.document.onkeydown = this._onKeyDown.bind(this);

  this._loadInfo();
};

TimeEditor.prototype._createElements = function (parent) {
  this._base =
  this._createBase(parent);
  this._save =
  new Button  (this._base,  10,  10, 172, "save");
  this._save.enable(true);
  this._save.setOnClick(this.onSaveData.bind(this));
  this._calendar =
  new Calendar(this._base,  40,  10, this);
  this._calendar.setDate(DateUtil.prevSunDay(new Date()));
  var USER_SELECT_LENDERER =
  {
    label : function (item) { return item.name; },
    value : function (item) { return item.id;   }
  };

  // note: hide now
  this._user =
  new Select        (this._base, 230,  10, 172, USER_SELECT_LENDERER, true);
  this._user.setOnChange(this.onUserChanged.bind(this));
  this._user._element.style.display = 'none';

  this._panel =
  new InfoPanel     (this._base, 250,  10, 172, this);
  this._band_view =
  new BandView      (this._base,  10, 200, this);
};

TimeEditor.prototype._createBase = function (parent) {
  var e = document.createElement('div');

  e.style.position = "relative";
  e.style.top      = "0px";
  e.style.left     = "0px";

  e.style.overflow = "visible";

  parent.appendChild(e);

  return e;
};

// = Actions

TimeEditor.prototype._loadInfo = function () {
  this._controller.baseInfo(function(err, result) {
    if (!err) {
      this.onSuccessGetBaseInfo(result);
    } else {
      this.onFailureGetBaseInfo(err);
    }
  }.bind(this));
};

TimeEditor.prototype._saveData = function () {
  var user_id = this._user.value();
  var info = createUpdateWorkInfo(this._work_info, user_id);

  this._controller.update(info, function(err, result) {
    if (!err) {
      this.onSuccessUpdateWorkInfo(result);
    } else {
      this.onFailureUpdateWorkInfo(err);
    }
  }.bind(this));
};

// =

TimeEditor.prototype._getWorkInfo = function () {
  var user_id = this._user.value();
  if (user_id < 0) {
    return;
  }

  this._band_view.invalidate();

  var s_date = this._calendar.getDate();
  var e_date = DateUtil.nextDate(s_date, this.OFFSET_DATE);

  this._band_view.setStartDate(s_date);

  var cond = {
    user_id : user_id,
    from: DateUtil.dateString(s_date),
    to: DateUtil.dateString(e_date)
  };
  this._controller.edit(cond, function (err, result) {
    if (!err) {
      this.onSuccessGetWorkInfo(result);
    } else {
      this.onFailureGetWorkInfo(err);
    }
  }.bind(this));
};

// = Internal Operation

TimeEditor.prototype._removeIfSelecting = function () {
  var info = this._band_view.getSelectingInfo();
  if (info == null) {
    return false;
  }

  var ret = window.confirm(this.MESSAGE_CONFIRM_DELETE_SELECTING);
  if (ret == false) {
    return false;
  }

  this._panel.setInfo();
  this._band_view.removeSelecting();

  return true;
};

// from Key

TimeEditor.prototype._moveSelectingUp = function () {
  this._band_view.moveSelectingUp();
};

TimeEditor.prototype._moveSelectingDown = function () {
  this._band_view.moveSelectingDown();
};

TimeEditor.prototype._selectNext = function () {
  this._band_view.selectNext();
};

TimeEditor.prototype._selectPrev = function () {
  this._band_view.selectPrev();
};

// = Handlers

// = from User

TimeEditor.prototype._onKeyDown = function (e) {
  if (e.ctrlKey == true) {
    return false;
  } else if (e.keyCode === 46 || e.keyCode === 8) {
    return !this._removeIfSelecting();
  }
  return true;
};

// from Button

TimeEditor.prototype.onSaveData = function () {
  this.onBlurSelecting();
  this._saveData();
};

// from Calendar

TimeEditor.prototype.onDateSelect = function (date) {
  this._getWorkInfo();
};

// from User Select

TimeEditor.prototype.onUserChanged = function () {
  var user_id = this._user.value();
  if (user_id == -1) {
    this._band_view.invalidate();
    this._band_view.clear();
    return;
  }

  this._getWorkInfo();
};

// from Pannel

TimeEditor.prototype.onWorkInfoChanged = function (info) {
  this._band_view.setSelectingInfo(info);
};

// from Band View

TimeEditor.prototype.onFocusSelecting = function (info) {
  this._panel.setInfo(info);
};

TimeEditor.prototype.onBlurSelecting = function () {
  var info = this._panel.getInfo();

  this._band_view.setSelectingInfo(info);
};

// from Control

TimeEditor.prototype.onSuccessGetBaseInfo = function (info) {
  console.dir(info);

  this._base_info = info;

  this._user.options(info.users);
  this._user.value(info.current_user.id);
  this._panel.baseInfo(info);
  this._band_view.baseInfo(info);

  this._user.enable(true);

  this._getWorkInfo();
};

TimeEditor.prototype.onFailureGetBaseInfo = function (reason) {
  alert("failed to get base info." + reason.toString());
};

TimeEditor.prototype.onSuccessGetWorkInfo = function (info) {
  function toStringKey(e) {
    return (e !== null) ? (String(e)) : ('-1');
  }

  function toWork(e) {
    e.user_id     = toStringKey(e.user_id);
    e.project_id  = toStringKey(e.project_id);
    e.issue_id    = toStringKey(e.issue_id);
    e.activity_id = toStringKey(e.activity_id);
    return e;
  }

  info = info.map(toWork);
  this._work_info = info;

  this._band_view.setData(info);
  this._band_view.validate();
};

TimeEditor.prototype.onFailureGetWorkInfo = function (reason) {
  alert("failed to get work info." + reason.toString());
};

TimeEditor.prototype.onSuccessUpdateWorkInfo = function () {
  this._getWorkInfo();
};

TimeEditor.prototype.onFailureUpdateWorkInfo = function (reason) {
  alert("failed to update work info." + reason.toString());
};
