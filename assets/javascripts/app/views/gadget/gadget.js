function setIfNotNull(operand, value)
{
	if ((operand == null) ||
		(value == null))
	{
		return;
	}

	opearand = value;
}

/**
 * @class Gadget
 *
 */
var Gadget =
{
	// フォントのサイズ
//	FONT_SIZE : '14px',

	// フォントの種別
//	FONT_FAMILY : 'sans-serif',

	// = public

	/**
	 * 有効化設定
	 * 
	 * @return なし
	 * 
	 * @param b	有効又は無効
	 */
	enable : function (b)
	{
	    this._element.disabled = !b;
	},

	/**
	 * 値の設定/取得
	 *
	 * @return 値
	 *
	 * @param value 値
	 */
	value : function(value)
	{
		if (value != null)
		{
			this._element.value = value;
		}

		return this._element.value;
	},

	/**
	 * 有効性の問合
	 *
	 * @return 有効/無効
	 * 
	 * 常にtrueを返す。
	 */
	isValid : function ()
	{
		return true;
	},

	/**
	 * 表示/非表示
	 *
	 * @return なし
	 *
	 * @param b 表示又は非表示
	 */
	visible : function(b)
	{
		this._element.style.display = b ? 'block' : 'none';
	},

	/**
	 * 背景色の設定/取得
	 *
	 * @return 色
	 *
	 * @param color 色
	 */
	backgroundColor : function(color)
	{
		if (color != null)
		{
			this._element.style.backgroundColor = color;
		}

		return this._element.style.backgroundColor;
	},

	/**
	 * 値変更ハンドラの設定
	 * 
	 * @return なし
	 *
	 * @param f ハンドラ
	 */
	setOnValueChange : function (f)
	{
	    this._onValueChange = f;
	},

	/**
	 * クリックハンドラの設定
	 * 
	 * @return なし
	 *
	 * @param f ハンドラ
	 */
	setOnClick : function (f)
	{
	    this._element.onclick = f;
	},

	// = protected

	/**
	 * クライアント領域における位置の取得
	 *
	 * @return なし
	 *
	 * @param element エレメント
	 */
	_clientPosition : function (element)
	{
		var x = 0;
		var y = 0;

		for (var node = element; node.tagName != 'BODY'; node = node.parentNode)
		{
			x += (node.offsetLeft);
			y += (node.offsetTop);
		}

		return { x : x, y : y };
	}
};

