/**
 * @class Calendar
*/

function Calendar(parent, top, left, handler) {
	// ui
  var height = parseInt(this.HEIGHT);
  var width  = parseInt(this.WIDTH);
  var bleft  = (width - 2) - 20 - 5;
  this._base  = this._createBase(parent, top, left, height, width);
  this._title = this._createTitle(this._base,   6, 0, 30, (width - 2));
  this._prev  = this._createButton(this._base,  5, 5, 20, 20,
                  'l', this._onPrev.bind(this));
  this._next  = this._createButton(this._base,  5, bleft, 20, 20,
                  'r', this._onNext.bind(this));
  this._table = this._createTable(this._base, 30, 0);
  this._handler = handler;

  // model
  this.setDate(new Date());
}

Calendar.prototype.HEIGHT = '199px';

Calendar.prototype.WIDTH  = '170px';

Calendar.prototype.CELL_HEIGHT = '20px';

Calendar.prototype.CELL_WIDTH = '24px';

Calendar.prototype.DAY_OF_THE_WEEK = [
  'So', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'
];

Calendar.prototype.show = function () {
  this._base.style.visibility = 'visible';
};

Calendar.prototype.hide = function () {
  this._base.style.visibility = 'hidden';
};

Calendar.prototype.setDate = function (date) {
	this._selectedDate  = date;
  this._year  = date.getFullYear();
  this._month = date.getMonth();

  this._refresh();
};

Calendar.prototype.getDate = function () {
  return this._selectedDate;
};

// = private UI

Calendar.prototype._createBase = function (parent, top, left, height, width) {
  var e = document.createElement('div');
  e.classList.add('calendar');

  e.style.position = "absolute";
  e.style.top    = String(top)    + 'px';
  e.style.left   = String(left)   + 'px';
  e.style.height = String(height) + 'px';
  e.style.width  = String(width)  + 'px';

  e.style.borderWidth = '1px';
  e.style.borderStyle = 'solid';
  e.style.borderColor = 'black';

  e.style.zIndex = 10;

  parent.appendChild(e);

  return e;
};

Calendar.prototype._createTitle = function (parent, top, left, height, width) {
  var e = document.createElement('div');
  e.classList.add('year-month');

  e.style.position = "absolute";
  e.style.top    = String(top)    + 'px';
  e.style.left   = String(left)   + 'px';
  e.style.height = String(height) + 'px';
  e.style.width  = String(width)  + 'px';

  e.style.textAlign   = 'center';
  e.style.cursor      = 'default';

  parent.appendChild(e);

  return e;
};

Calendar.prototype._createButton = function (parent, top, left, height, width, direction, ondown) {
  var e = document.createElement('div');

  e.style.position = "absolute";
  e.style.top    = String(top)    + 'px';
  e.style.left   = String(left)   + 'px';
  e.style.height = String(height) + 'px';
  e.style.width  = String(width)  + 'px';

  e.style.overflow = 'hidden';

  e.style.borderWidth = '1px';
  e.style.borderStyle = 'solid';
  e.style.borderColor = 'ivory';

  e.style.cursor      = 'default';

  parent.appendChild(e);

  e.onmouseover = function () { this.style.borderColor = 'orange'; };
  e.onmouseout  = function () { this.style.borderColor = 'ivory';  };

  e.onmousedown = ondown;

  // ---------------------------------------------------------------------

  var LARROW = [
    [12, 1 ],
    [11, 3 ],
    [10, 3 ],
    [ 9, 3 ],
    [ 8, 3 ],
    [ 9, 3 ],
    [10, 3 ],
    [11, 3 ],
    [12, 1 ]
  ];

  var RARROW = [
    [10, 1 ],
    [ 9, 3 ],
    [10, 3 ],
    [11, 3 ],
    [12, 3 ],
    [11, 3 ],
    [10, 3 ],
    [ 9, 3 ],
    [10, 1 ]
  ];

  var ARROW = (direction == 'l') ? (LARROW) : (RARROW);

  for (var i = 0; i < ARROW.length; i++) {
    var b1 = document.createElement('div');

    b1.style.position = 'absolute';
    b1.style.top   = String((i + 5)) + 'px';
    b1.style.left  = String(ARROW[i][0]) + 'px';
    b1.style.height= '1px';
    b1.style.width = String(ARROW[i][1]) + 'px';

    b1.style.overflow = 'hidden';
    b1.style.backgroundColor = 'black';

    e.appendChild(b1);

    var b2 = document.createElement('div');

    b2.style.position = 'absolute';
    b2.style.top   = String((i + 5)) + 'px';
    b2.style.left  = String(ARROW[i][0] - 5) + 'px';
    b2.style.height= '1px';
    b2.style.width = String(ARROW[i][1]) + 'px';

    b2.style.overflow = 'hidden';
    b2.style.backgroundColor = 'black';

    e.appendChild(b2);
  }

  return e;
};

Calendar.prototype._createTable = function (parent, top, left) {
  // table
  var table = document.createElement('table');
  var tbody = document.createElement('tbody');

  table.style.position = "absolute";
  table.style.top    = String(top)   + 'px';
  table.style.left   = String(left)  + 'px';

  table.cellPadding = '0px';
  table.cellSpacing = '0px';

  table.style.tableLayout = 'fixed';

  table.style.textAlign   = 'center';
  table.style.cursor      = 'default';

  table.appendChild(tbody);
  parent.appendChild(table);

  // header
  var thead = table.createTHead();
  thead.classList.add('day-of-week');

  var row = thead.insertRow(0);
  for (var i = 0; i < this.DAY_OF_THE_WEEK.length; i++) {
    var cell = row.insertCell(i);

    cell.style.height = this.CELL_HEIGHT;
    cell.style.width  = this.CELL_WIDTH;

    cell.innerText = this.DAY_OF_THE_WEEK[i];
  }

  // body
  var d = 0;
  for (var r = 0; r < 6; r++) {
    var row = tbody.insertRow(r);

    for (var c = 0; c < this.DAY_OF_THE_WEEK.length; c++) {
      var cell= row.insertCell(c);
      cell.classList.add('day');

      cell.style.height = this.CELL_HEIGHT;
      cell.style.width  = this.CELL_WIDTH;
      cell.addEventListener('click', this._onCellDown.bind(this, cell), true);
    }
  }

  return table;
};

// = private Model

Calendar.prototype._refresh = function () {
  // year-month
  this._title.innerHTML = this._year + '/' + (this._month + 1);

  // table
  var date = DateUtil.prevSunDay(new Date(this._year, this._month, 1));
  for (var row = 1; row < 7; row++) {
    for (var col = 0; col < 7; col++) {
      var cell = this._table.rows[row].cells[col];

      cell.classList.remove('selected-day');
      cell.classList.remove('day-of-this-month');
      cell.classList.remove('day-of-other-month');
      var attr = (date.getMonth() === this._month) ?
                 ('day-of-this-month') :
                 ('day-of-other-month');
      cell.classList.add(attr);
      if (DateUtil.IsSameDate(date, this._selectedDate)) {
        cell.classList.add('selected-day');
      }
      cell.innerText  = date.getDate();
      cell.dateString = date.toString();

      date = DateUtil.nextDate(date);
    }
  }
};

// = Handler

Calendar.prototype._onPrev = function () {
  this._month--;

  if (this._month == -1) {
    this._month = 11;
    this._year--;
  }

  this._refresh();
};

Calendar.prototype._onNext = function () {
  this._month++;

  if (this._month == 12) {
    this._month = 0;
    this._year++;
  }

  this._refresh();
};

Calendar.prototype._onCellDown = function (cell) {
	this.setDate(new Date(cell.dateString));

	this._handler.onDateSelect(this._selectedDate);
};
