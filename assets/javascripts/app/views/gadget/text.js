function Text(parent, top, left, width, value, attr) {
	if (attr) {
		S.extend(this, attr);
	}

	var e = document.createElement('div');

	e.style.position = "absolute";
	e.style.top      = String(parseInt(top)   + 2) + 'px';
	e.style.left     = String(parseInt(left))      + 'px';
	e.style.width    = String(parseInt(width))     + 'px';

	setIfNotNull(e.style.backgroundColor, this.BACKGROUND_COLOR);
	setIfNotNull(e.style.borderStyle,     this.BORDER_STYLE);
	setIfNotNull(e.style.borderWidth,     this.BORDER_WIDTH);
	setIfNotNull(e.style.borderColor,     this.BORDER_COLOR);
	setIfNotNull(e.style.overflow,        this.OVERFLOW);
	setIfNotNull(e.style.textAlign,       this.TEXT_ALIGN);

	parent.appendChild(e);

	this._element = e;

	this.value(value);
}

S.extend(Text.prototype, Gadget);

// Constants

Text.prototype.BACKGROUND_COLOR = null;

// =

Text.prototype.BORDER_STYLE = null;

Text.prototype.BORDER_WIDTH = null;

Text.prototype.BORDER_COLOR = null;

// =

Text.prototype.FONT_COLOR = 'black';

Text.prototype.OVERFLOW = null;

Text.prototype.TEXT_ALIGN = 'left';

// = public

Text.prototype.value = function(value) {
  if (value != null) {
    this._element.innerHTML = value.toString();
  }
  return this._element.innerText;
};
