/**
 * @class DateSelect
 */

function DateSelect(parent, top, left, width) {
	this._base = this._createElement(parent, top, left, width);

	this._input = this._createInput(this._base, 0, 0, width);

	var client = this._clientPosition(this._base);
	var ctop  = client.y + parseInt(this.HEIGHT);
	var cleft = client.x +
    Math.floor((width - parseInt(Calendar.prototype.WIDTH)) / 2);
	this._calendar = new Calendar(document.body, ctop, cleft, this);

	this._date = new Date();

	this._input.value = this._date.toUTCDateString();
}

S.extend(DateSelect.prototype, Gadget);

// = public

DateSelect.prototype.value = function(value) {
	if (value != null) {
		value = new Date(value.toString());

		this._date = value;
		this._input.value = value.toUTCDateString();
	}

	return new Date(this._date.toString());
};

DateSelect.prototype.enable = function (b) {
	this._input.disabled = (!b);
};

DateSelect.prototype.isValid = function () {
	return true;
};

// = private

DateSelect.prototype._createElement = function (parent, top, left, width) {
	var e = document.createElement('div');

	e.style.position = "absolute";
	e.style.top    = String(top)   + 'px';
	e.style.left   = String(left)  + 'px';

	e.style.zIndex = 1;

	e.style.cursor = 'default';

	parent.appendChild(e);

	return e;
};

DateSelect.prototype._createInput = function (parent, top, left, width) {
	var s = document.createElement('input');
	s.type = 'text';

	s.style.position = "absolute";
	s.style.top    = String(top)   + 'px';
	s.style.left   = String(left)  + 'px';
	s.style.height = this.HEIGHT;
	s.style.width  = String(width) + 'px';

	s.style.fontSize   = this.FONT_SIZE;
	s.style.fontFamily = this.FONT_FAMILY;

	s.onmousedown = this._onMouseDown.bind(this);

	s.style.cursor = 'default';

	s.readOnly = true;

	parent.appendChild(s);

	return s;
};

DateSelect.prototype.onSelect = function (date) {
	this.value(date);

	if (this._onValueChange == null) {
		return;
	}

	this._onValueChange();
};

DateSelect.prototype._onMouseDown = function () {
	this._calendar.show(this._date);
};
