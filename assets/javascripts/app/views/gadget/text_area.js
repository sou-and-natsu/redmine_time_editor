/**
 * @class TextArea
 */

function TextArea(parent, top, left, height, width) {
  this._element = this._createElement(parent, top, left, height, width);
  this.enable(false);
}

S.extend(TextArea.prototype, Gadget);

// = public

TextArea.prototype.isValid = function () {
	return true;
};

TextArea.prototype.readonly = function (which) {
	this._element.readOnly = which;
};

// = private

TextArea.prototype._createElement = function (parent, top, left, height, width) {
	var s = document.createElement('textarea');

	s.style.position = "absolute";
	s.style.top    = String(top)   + 'px';
	s.style.left   = String(left)  + 'px';
	s.style.height = String(height)+ 'px';
	s.style.width  = String(width) + 'px';
  s.style.resize = 'none';

	s.style.imeMode = 'active';
	s.addEventListener('keydown', this._onKeyDown.bind(this));
	s.addEventListener('input', this._onPropertyChange.bind(this), false);

	parent.appendChild(s);

	return s;
};

// =

TextArea.prototype._onKeyDown = function (e) {
	e.stopPropagation();
  return true;
}

TextArea.prototype._onPropertyChange = function () {
  return true;
};
