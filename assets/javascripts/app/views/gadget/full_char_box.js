function ByteLength(s) {
	var length = 0;

	var es = escape(s);
	for (var i = 0; i < es.length; i++) {
		if (es.charAt(i) == "%") {
			if (es.charAt(++i) == "u") {
				i += 3;
				length++;
			}
			i++;
		}
		length++;
	}
	return length;
}

/**
 * @class FullCharBox
 */

function FullCharBox(parent, top, left, width, notNull, maxLength) {
  this._element = this._createElement(parent, top, left, width);
  this._notNull = notNull;
  this._maxByteLength = maxLength * 2;
  this.enable(false);
}

S.extend(FullCharBox.prototype, Gadget);

// = public

FullCharBox.prototype.isValid = function () {
	var value = this._element.value;

	if (value.length === 0) {
		return (!this._notNull);
	}

	return true;
};

FullCharBox.prototype.readonly = function (which) {
	this._element.readOnly = which;
};

// = private

FullCharBox.prototype._createElement = function (parent, top, left, width) {
	var s = document.createElement('input');
	s.type = 'text';

	s.style.position = "absolute";
	s.style.top    = String(top)   + 'px';
	s.style.left   = String(left)  + 'px';
	s.style.width  = String(width) + 'px';

	setIfNotNull(s.style.fontSize,   this.FONT_SIZE);
	setIfNotNull(s.style.fontFamily, this.FONT_FAMILY);

	s.style.imeMode = 'active';

	s.addEventListener('keydown', this._onKeyDown.bind(this));
	s.addEventListener('input', this._onPropertyChange.bind(this), false);

	parent.appendChild(s);

	return s;
};

	// =

FullCharBox.prototype._onPropertyChange = function () {
  console.log('FullCharBox._onPropertyChange');

	this._element.value = this._normalize(this._element.value);

//	this._onValueChange(this);
};

	// =

FullCharBox.prototype._normalize = function (value) {
	var r = '';

	var length = 0;
	for (var i = 0; i < value.length; i++) {
		var c = value.charAt(i);
		length += (c != escape(c)) ? (2) : (1);

		if (length > this._maxByteLength) {
			break;
		}
		r += c;
	}

	return r;
};

FullCharBox.prototype._onKeyDown = function (e) {
	console.log('FullCharBox._onKeyDown');

	e.stopPropagation();
  return true;
}
