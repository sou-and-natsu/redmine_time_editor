/**
 * @class CheckBox
 *
 */
CheckBox = Class.create();

Object.extend(CheckBox.prototype, Gadget);
Object.extend(CheckBox.prototype,
{
	// 幅
	WIDTH : 20,

	/**
	 * コンストラクタ
	 * 
	 * @return なし
	 * 
	 * @param top	トップ
	 * @param left	レフト
	 * @param width	幅
	 * @param value	値
	 */
	initialize : function (parent, top, left, width, text)
	{
		// チェックボックス
		var s = document.createElement('input');

		s.type = 'checkbox';

		s.style.position = "absolute";
		s.style.top      = String(parseInt(top)) + 'px';
		s.style.left     = String(parseInt(left)) + 'px';

		parent.appendChild(s);

		this._element = s;

		// テキスト
		var t = document.createElement('div');

		t.style.position = "absolute";
		t.style.top      = String(parseInt(top) + 2) + 'px';
		t.style.left     = String(parseInt(left) + this.WIDTH) + 'px';

		t.style.width = String(parseInt(width) - this.WIDTH) + 'px';

		t.style.fontSize   = this.FONT_SIZE;
		t.style.fontFamily = this.FONT_FAMILY;

		t.innerText = text.toString();

		parent.appendChild(t);

		this._text = t;

		this.enable(false);
	},

	/**
	 * 値の設定/取得
	 *
	 * @return 値
	 *
	 * @param value 値
	 */
	value : function(value)
	{
		if (null != value)
		{
			this._element.checked = (value > 0);
		}

		return (this._element.checked) ? (1) :(0);
	}

});

