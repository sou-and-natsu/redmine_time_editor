﻿/**
 * @class Div
*/

function Div(parent, rect, style) {
  this._parent  = parent;
  this._element = document.createElement("div");

  this._element.style.position = "absolute";

  S.extend(this._element.style, style);

  this._top   (rect.top);
  this._left  (rect.left);
  this._height(rect.height);
  this._width (rect.width);

  this._element.style.fontSize = "10px";

  parent.appendChild(this._element);
}

// =

Div.prototype.finalize = function () {
  this._parent.removeChild(this._element);

  delete this._element;
};

// = Base Operation

Div.prototype.setText = function (text) {
  this._element.innerHTML = text;
};

// = Base Operation

Div.prototype.top = function () {
  return parseInt(this._element.style.top);
};

Div.prototype.bottom = function () {
  return (this.top() + this.height());
};

Div.prototype.height = function () {
  return (parseInt(this._element.style.height) + this._offsetHeight());
};

Div.prototype.left = function () {
  return parseInt(this._element.style.left);
};

Div.prototype.right = function () {
  return (this.left() + this.width());
};

Div.prototype.width = function () {
  return (parseInt(this._element.style.width) + this._offsetWidth());
};

// =

Div.prototype._offsetHeight = function () {
  if ((this._element.style.borderWidth == null) ||
      (this._element.style.borderWidth == ""))
  {
    return 0;
  }

  return parseInt(this._element.style.borderWidth) * 2;
};

Div.prototype._offsetWidth = function () {
  if ((this._element.style.borderWidth == null) ||
      (this._element.style.borderWidth == "")) {
    return 0;
  }

  return parseInt(this._element.style.borderWidth) * 2;
};

// =

Div.prototype._top = function (v) {
  this._element.style.top = "" + v + "px";
};

Div.prototype._height = function (v) {
  var offset = this._offsetHeight();

  this._element.style.height = "" + (v - offset) + "px";
};

Div.prototype._left = function (v) {
  this._element.style.left = "" + v + "px";
};

Div.prototype._width = function (v) {
  var offset = this._offsetWidth();

  this._element.style.width = "" + (v - offset) + "px";
};

// = Moving To

Div.prototype.moveTopTo = function (top) {
  var height = this.bottom() - top;

  this._top(top);
  this._height(height);
};

Div.prototype.moveBottomTo = function (bottom) {
  var height = bottom - this.top();

  this._height(height);
};

// = Moving By

Div.prototype.moveBy = function (x, y) {
  var top  = this.top()  + y;
  var left = this.left() + x;

  this._top(top);
  this._left(left);
};

Div.prototype.moveTopBy = function (y) {
  var top    = this.top()    + y;
  var height = this.height() - y;

  this._top(top);
  this._height(height);
};

Div.prototype.moveBottomBy = function (y) {
  var height = this.height() + y;

  this._height(height);
};

// = Attribute Operation

Div.prototype.setColor = function (color) {
  this._element.style.backgroundColor = color;
};

Div.prototype.hitTest = function (pos) {
  var res = 0;

  var x = pos.x;

  var l_pos = this.left();
  var r_pos = this.right();

  if ((x < l_pos) || (x > r_pos)) {
    return res;
  }

  var y = pos.y;

  var top    = this.top();
  var bottom = this.bottom();
  if ((y >= (top + 0)) && (y <= (top + 2))) {
    res = 1;
  } else if ((y >= (bottom - 2)) && (y <= (bottom - 0))) {
    res = 2;
  }
  else if ((y >= top) && (y <= bottom)) {
    res = 3;
  }

  return res;
};
