/**
 * @class InfoTable
 *
 * @brief 情報テーブル
 * 
 * 基本情報を表示し、ページ操作が可能
 */
InfoTable = Class.create();

Object.extend(InfoTable.prototype,
{
	// = フォントの属性

	// フォントのサイズ
	FONT_SIZE   : '14px',

	// フォントの種別
	FONT_FAMILY : 'sans-serif',

	// 文字色
	FONT_COLOR : 'black',

	// テキストの表示位置
	TEXT_ALIGN : 'left',

	// = 行の属性

	// 行数
	ROW_NUM    : 15,

	// ヘッダカラー
	HEADER_ROW_HEIGHT : '21px',

	// 行の高さ
	ROW_HEIGHT : '40px',

	// = 色

	// ヘッダカラー
	TABLE_HEADER_COLOR   : 'lightgreen',

	// ボディカラー
	TABLE_BODY_COLOR     : 'ivory',

	// ボーダーカラー
	TABLE_BORDER_COLOR   : 'grey',

	// 選択中のカラー
	TABLE_SELECTED_COLOR : 'cyan',

	// = スクロール

	// スクロール有効性
	SCROLLABLE : true,

	// = ソート関連

	// ソートキー
	KEY : null,

	/**
	 * コンストラクタ
	 * 
	 * @param top	トップ
	 * @param left	レフト
	 * @param cols	列情報
	 */
	initialize : function (parent, top, left, cols, attr, handler)
	{
		Object.extend(this, attr);

		// 列情報
		this._cols = cols;

		// ヘッダ部
		this._head      = this._createHead(top, left);
		this._headTable = this._createHeadTable(this._head);

		// ボディ部
		this._body      = this._createBody(top, left);
		this._bodyTable = this._createBodyTable(this._body);

		parent.appendChild(this._head);
		parent.appendChild(this._body);
		
		this._infoIndex = -1;
		this._selectedRowHandler = null;

		this._handler = handler;
	},

	/**
	 * 情報の設定
	 * 
	 * @return なし
	 */
	setInfo : function (infos)
	{
		this._clearInfo();

		for (var i = 0; i < infos.length; i++)
		{
			this.addInfo(infos[i]);
		}
	},

	/**
	 * 情報の追加
	 * 
	 * @return なし
	 * 
	 * @param info	情報
	 */
	addInfo : function (info)
	{
		if (this._getIndex(info) > 0)
		{
			return;
		}

		this.selectRow();

		this._infos.push(info);

		// ソート用ファンクタ
		var key = this._key();
		var SORT_FUNC = function (a, b)
		{
			return (a[key] < b[key]) ? (-1) : (1);
		}
		this._infos = this._infos.sort(SORT_FUNC);

		var index = this._infos.indexOf(info);
		var row   = this._bodyTable.insertRow(index);

		var _this = this;
		row.height = String(this.ROW_HEIGHT) + 'px';

        row.style.backgroundColor = this.TABLE_BODY_COLOR;

      	for(var i = 0; i < this._cols.length; i++)
      	{
			var cell = row.insertCell(i);
			cell.width = String(this._cols[i].width) + 'px';

			this._setupBodyCell(cell, info, i);
		}

		this._bodyTable.rows[index].onmousedown = 
			this._onRowMouseDown.bind(this, this._bodyTable.rows[index]);
	},

	/**
	 * 情報の変更
	 * 
	 * @return なし
	 * 
	 * @param index	インデックス
	 * @param info	情報
	 */
	updateInfo : function (info)
	{
		var index = this._getIndex(info);
		if (index < 0)
		{
			return;
		}

		this._infos[index] = info;

		var row = this._bodyTable.rows[index];

      	for(var i = 0; i < this._cols.length; i++)
      	{
			var cell = row.cells[i];

			this._setupBodyCell(cell, info, i);
		}
	},

	/**
	 * 情報の削除
	 * 
	 * @return なし
	 * 
	 * @param index	インデックス
	 */
	removeInfo : function (info)
	{
		var index = this._getIndex(info);
		if (index < 0)
		{
			return;
		}

		this._bodyTable.rows[index].onmousedown = null;

		this._infos.splice(index, 1);
		this._bodyTable.deleteRow(index);
		
		if (this._infoIndex == index)
		{
			this._infoIndex = -1;
		}
	},

	// =

	/**
	 * 選択中の情報の取得
	 * 
	 * @return 選択中の情報
	 */
	hasSelected : function ()
	{
		return (this._infoIndex >= 0);
	},

	/**
	 * 選択中の情報の取得
	 * 
	 * @return 選択中の情報
	 */
	getSelectedInfo : function ()
	{
		if (this._infoIndex < 0)
		{
			return null;
		}

		return this._infos[this._infoIndex];
	},

	/**
	 * 行の選択
	 *
	 * @return なし
	 *
	 * @param index インデクス
	 */
	selectRow : function (index)
	{
		if (index == null)
		{
			index = -1;
		}

		if (index >= this._bodyTable.rows.length)
		{
			return;
		}

		if (index == this._infoIndex)
		{
			return;
		}

		// 以前に選択されていた行の処理
		if (this._infoIndex >= 0)
		{
			var old = this._bodyTable.rows[this._infoIndex];

			old.style.backgroundColor = this.TABLE_BODY_COLOR;
		}

		// 新たに選択された行の処理
		this._infoIndex = index;
		if (this._infoIndex >= 0)
		{
			var row = this._bodyTable.rows[this._infoIndex];

			row.style.backgroundColor = this.TABLE_SELECTED_COLOR;
		}

		// ハンドラのコール
		if (this._handler == null)
		{
			return;
		}

		if (this._handler.onSelectedChange == null)
		{
			return;
		}

		var info = this.getSelectedInfo();

		this._handler.onSelectedChange(info);
	},

	// = 

	/**
	 * キーの取得
	 *
	 * @return キー
	 */
	_key : function ()
	{
		if (this.KEY == null)
		{
			return this._cols[0].id;
		}

		return this.KEY;
	},

	/**
	 * 該当情報のインデックスの取得
	 * 
	 * @return 該当情報のインデックス
	 *
	 * @param info 情報（キーのみ）
	 */
	_getIndex : function (info)
	{
		var key = this._key();

		var t = info[key];
		if (t == null)
		{
			return -1;
		}
		var target = this._infos.find(function (v, i)
		{
			return (v[key] == t); 
		});

		return this._infos.indexOf(target);
	},

	/**
	 * 情報のクリア
	 * 
	 * @return なし
	 */
	_clearInfo : function()
	{
		var rowsNum = this._bodyTable.rows.length;
		for (var i = (rowsNum - 1); i >= 0; i--)
		{
			this._bodyTable.rows[i].onmousedown = null;
			this._bodyTable.deleteRow(i);
		}

		this._infos = [];
		this._infoIndex = -1;
	},

	// = 生成

	/**
 	 * ヘッダの生成
	 * 
	 * @return ヘッダ領域
	 */
	_createHead : function(top, left)
	{
		// ヘッダレイヤ
		var layer = document.createElement('div');

		layer.style.position = 'absolute';
		layer.style.top    = String(parseInt(top)) + 'px';
		layer.style.left   = String(parseInt(left)) + 'px';
		layer.style.height = this._headHeight();
		layer.style.width  = this._headWidth();

		layer.style.overflow = 'hidden';

		layer.style.backgroundColor = 'lightgrey';

		return layer;
	},

	/**
 	 * テーブルの生成
	 * 
	 * @return テーブル
	 */
	_createHeadTable : function(layer)
	{
	    var table = document.createElement('table');
	    var tbody = document.createElement('tbody');

        // 位置
	    table.style.position = 'absolute';
	    table.style.top    = '0px';
	    table.style.left   = '0px';
		layer.style.height = this.HEADER_ROW_HEIGHT;
	    table.style.width  = this._tableWidth();

        // 見た目
		table.style.tableLayout    = 'fixed';
		table.style.textAlign      = 'center';

		table.style.borderCollapse = "collapse";
        table.style.borderStyle    = "solid";
        table.style.borderWidth    = "1px";
        table.style.borderColor    = "mistyrose";

        // フォント
		table.style.fontFamily = this.FONT_FAMILY;
		table.style.fontSize   = this.FONT_SIZE;

        // =

      	var row = table.insertRow(0);
      	row.style.height = this.HEADER_ROW_HEIGHT;

      	for(var i = 0; i < this._cols.length; i++)
      	{
	        var cell = row.insertCell(i);

			cell.style.width           = "" + this._cols[i].width + "px";
            cell.style.backgroundColor = "palegreen";

            cell.style.borderStyle    = "solid";
            cell.style.borderWidth    = "1px";
            cell.style.borderColor    = "mistyrose";

	        var value = this._cols[i].label;
	        cell.innerHTML = (value.length != 0) ? (value) : ('<br>');
		}

	    table.appendChild(tbody);
	    layer.appendChild(table);

		return table;
	},

	/**
 	 * ボディの生成
	 * 
	 * @return ボディ領域
	 */
	_createBody : function(top, left)
	{
		// ボディレイヤ
		var layer = document.createElement('div');

		layer.style.position = 'absolute';
		layer.style.top    = String(parseInt(top) + parseInt(this.HEADER_ROW_HEIGHT)) + 'px';
		layer.style.left   = String(parseInt(left)) + 'px';
		layer.style.height = this._bodyHeight();
		layer.style.width  = this._bodyWidth();

		layer.style.overflowX = 'hidden';
		layer.style.overflowY = (this.SCROLLABLE) ? ('scroll') : ('hidden');

		layer.style.backgroundColor = 'lightsteelblue';

/*
		layer.style.borderWidth = '1px';
		layer.style.borderStyle = 'solid';
		layer.style.borderColor = this.TABLE_BORDER_COLOR;
*/

		layer.onmousedown = this._onBodyMouseDown.bind(this);

		return layer;
	},

	/**
 	 * テーブルの生成
	 * 
	 * @return テーブル
	 */
	_createBodyTable : function(layer)
	{
		// ボディテーブル
	    var table = document.createElement('table');
	    var tbody = document.createElement('tbody');

	    table.style.position = "absolute";
	    table.style.top    = '0px';
	    table.style.left   = '0px';
	    table.style.width  = this._tableWidth();

		table.style.tableLayout = 'fixed';

		table.style.textAlign   = this.TEXT_ALIGN;

		table.style.borderCollapse = "collapse";
        table.style.borderStyle    = "solid";
        table.style.borderWidth    = "1px";
        table.style.borderColor    = "mistyrose";

		table.style.fontFamily = this.FONT_FAMILY;
		table.style.fontSize   = this.FONT_SIZE;
		table.style.color      = this.FONT_COLOR;

	    table.appendChild(tbody);
		layer.appendChild(table);

		return table;
	},

	/**
	 * セルのセットアップ
	 *
	 * @return なし
	 */
	_setupBodyCell : function (cell, info, index)
	{
		var id    = this._cols[index].id;
		var rule  = this._cols[index].rule;
		var value = info[id];

        cell.style.borderStyle    = "solid";
        cell.style.borderWidth    = "1px";
        cell.style.borderColor    = "mistyrose";

		if (rule)
		{
			rule(cell, value, info);
		}
		else
		{
			cell.innerHTML = (value.length != 0) ? (value.toString()) : ('<br>');
		}
	},

	// = サイズ

	/**
 	 * ヘッダの高さの取得
	 * 
	 * @return ヘッダの高さ
	 */
	_headHeight : function()
	{
		return String(parseInt(this.HEADER_ROW_HEIGHT) + 2)  + 'px';
	},

	/**
 	 * ヘッダの幅の取得
	 * 
	 * @return ヘッダの幅
	 */
	_headWidth : function()
	{
		return String(parseInt(this._tableWidth())) + 'px';
	},

	/**
 	 * ヘッダテーブルの高さの取得
	 * 
	 * @return ヘッダの高さ
	 */
	_headTableHeight : function()
	{
		return String(parseInt(this.HEADER_ROW_HEIGHT) + 2)  + 'px';
	},

	/**
 	 * ヘッダテーブルの幅の取得
	 * 
	 * @return ヘッダの幅
	 */
	_headTableWidth : function()
	{
		return String(parseInt(this._tableWidth())) + 'px';
	},

	/**
 	 * ボディの高さの取得
	 * 
	 * @return ボディの高さ
	 */
	_bodyHeight : function()
	{
		return String(parseInt(this._tableHeight())) + 'px';
	},

	/**
 	 * ボディの幅の取得
	 * 
	 * @return ボディの幅
	 */
	_bodyWidth : function()
	{
		var bodyWidth = parseInt(this._tableWidth());

		if (this.SCROLLABLE)
		{
			bodyWidth += 17;
		}

		return String(bodyWidth) + 'px';
	},

	/**
 	 * 表の高さの取得
	 * 
	 * @return 表の高さ
	 */
	_tableHeight : function()
	{
		return String(this.ROW_NUM * parseInt(this.ROW_HEIGHT) + 2) + 'px';
	},

	/**
 	 * 表の幅の取得
	 * 
	 * @return 表の幅
	 */
	_tableWidth : function()
	{
		var width = 2;

		for(var i = 0; i < this._cols.length; i++)
		{
		  width += parseInt(this._cols[i].width) + 4;
		}

		return String(width) + 'px';
	},

	// = ハンドラ

	/**
 	 * 行マウスダウン時のハンドラ
	 * 
	 * @return なし
	 */
	_onRowMouseDown : function(row, event)
	{
		if (Event.isLeftClick(event) == false)
		{
			return true;
		}

		this.selectRow(row.rowIndex);

		Event.stop(event);

		return false;
	},

	/**
 	 * テーブル背景領域マウスダウン時のハンドラ
	 * 
	 * @return なし
	 */
	_onBodyMouseDown : function (event)
	{
		this.selectRow();

		Event.stop(event);

		return false;
	},

	// = データメンバ

	// 列情報
	_cols : null,

	// ヘッダ部
	_head : null,

	// ヘッダテーブル
	_headTable : null,

	// ボディ部
	_body : null,

	// ボディテーブル
	_bodyTable : null,

	// 情報
	_infos : [],

	// 情報インデクス
	_infoIndex : -1

});

