function HalfCharBox(parent, top, left, width, notNull, maxLength) {
	this._element = this._createElement(parent, top, left, width);
	this._notNull = notNull;
	this._maxLength = maxLength;

	this.enable(false);
}

S.extend(HalfCharBox.prototype, Gadget);

// = public

HalfCharBox.prototype.isValid = function () {
	var value = this._element.value;

	if (value.length == 0) {
		return (!this._notNull);
	}

	return true;
};

HalfCharBox.prototype.readonly = function (which) {
	this._element.readOnly = which;
};

// = private

HalfCharBox.prototype._createElement = function (parent, top, left, width) {
	var s = document.createElement('input');
	s.type = 'text';

	s.style.position = "absolute";
	s.style.top    = String(top)   + 'px';
	s.style.left   = String(left)  + 'px';
	s.style.width  = String(width) + 'px';

	s.style.imeMode = 'disabled';

	s.onkeydown = this._onKeyDown.bind(this);
	s.addEventListener('input', this._onPropertyChange.bind(this), false);

	parent.appendChild(s);

	return s;
};

HalfCharBox.prototype._onKeyDown = function (e) {
	e.stopPropagation();

	var keyCode = e.keyCode;

	if ((keyCode == Event.KEY_BACKSPACE) ||
		(keyCode == Event.KEY_TAB) ||
		(keyCode == Event.KEY_RETURN) ||
		(keyCode == Event.KEY_ESC) ||
		(keyCode == Event.KEY_LEFT) ||
		(keyCode == Event.KEY_UP) ||
		(keyCode == Event.KEY_RIGHT) ||
		(keyCode == Event.KEY_DOWN) ||
		(keyCode == Event.KEY_DELETE)) {
		return true;
	}

	if (keyCode == 229) {
		return false;
	}

	var value = this._element.value;
	if (value.length >= this._maxLength) {
		return false;
	}

	return true;
};

HalfCharBox.prototype._onPropertyChange = function () {
	if (this._onValueChange == null) {
		return;
	}

	this._onValueChange(this);
};
