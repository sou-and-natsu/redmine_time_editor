﻿/**
 * @class
 */

function SimpleTable(parent, top, left, attr) {
  this._element = this._createElement(parent, top, left);
  S.extend(this._attr, attr);
};

// = Operations

SimpleTable.prototype.build = function (row_num , column_num) {
  var table = this._element;
  this._attr.setupTable(table, row_num, column_num);

  for (var i = 0; i < row_num; i++) {
    var row = table.insertRow(i);
    this._attr.setupRow(row, i);

    for (var j = 0; j < column_num; j++) {
      var cell = row.insertCell(j);
      this._attr.setupCell(cell, i, j);
    }
  }
};

SimpleTable.prototype.setText = function (row_index, column_index, text) {
  this._element.rows[row_index].cells[column_index].innerHTML = text;
};

SimpleTable.prototype.clear = function () {
  var rowsNum = this._element.rows.length;
  for (var i = (rowsNum - 1); i >= 0; i--) {
    this._element.deleteRow(i);
  }
};

// =

SimpleTable.prototype._createElement = function (parent, top, left) {
  var table = document.createElement("table");
  var tbody = document.createElement("tbody");

  table.style.position = "absolute";
  table.style.top      = "" + top  + "px";
  table.style.left     = "" + left + "px";

  table.style.tableLayout = "fixed";

  table.appendChild(tbody);
  parent.appendChild(table);

  return table;
};

// =

SimpleTable.prototype.applyRow = function (functor) {
  var table = this._element;

  for (var i = 0; i < table.rows.length; i++) {
    functor(table.rows[i], i);
  }
};

SimpleTable.prototype._attr = {
  setupTable : function (table, row_num, column_num) {},
  setupRow   : function (row, row_index) {},
  setupCell  : function (cell, row_index, column_index) {}
};
