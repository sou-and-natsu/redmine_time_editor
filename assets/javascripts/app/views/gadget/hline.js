/**
 * @class HLine
 */

function HLine(parent, top, left, width) {
  var e = document.createElement('hr');

  e.style.position = "absolute";
  e.style.top = String(parseInt(top)) 	+ 'px';
  e.style.left = String(parseInt(left)) 	+ 'px';
  e.style.width = String(parseInt(width)) + 'px';

  parent.appendChild(e);
}
