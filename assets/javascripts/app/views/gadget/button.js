/**
 * @class Button
 */

function Button(parent, top, left, width, value, onclick, attr) {
	if (attr) {
		S.extend(this, attr);
	}

	var e = document.createElement('input');

	e.type = 'button';

	e.style.position = "absolute";
	e.style.top    = String(parseInt(top)) + 'px';
	e.style.left   = String(parseInt(left)) + 'px';
	e.style.width  = String(parseInt(width)) + 'px';

	e.style.backgroundColor = this.BACKGROUND_COLOR;
	e.onclick     = onclick;
	e.onmouseover = this._changeMouseOver.bind(this);
	e.onmouseout  = this._chnageMouseOut.bind(this);

	parent.appendChild(e);

	this._element = e;

	this.value(value);
	this.enable(false);
}

S.extend(Button.prototype, Gadget);

Button.prototype.BACKGROUND_COLOR = "gainsboro";

Button.prototype.MOUSE_OVER_COLOR = "whitesmoke";

// = public

Button.prototype._changeMouseOver = function () {
  this._element.style.backgroundColor = this.MOUSE_OVER_COLOR;
};

Button.prototype._chnageMouseOut = function () {
  this._element.style.backgroundColor = this.BACKGROUND_COLOR;
};
