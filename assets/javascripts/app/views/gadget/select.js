/**
 * @class SelectFunctor
 */

var SelectFunctor =
{
  DEFAULT :
  {
    label : function (x) { return x; },
    value : function (x) { return x; }
  },

  LABEL_VALUE :
  {
    label : function (item) { return item.label; },
    value : function (item) { return item.value; }
  }

};

/**
 * @class Select
 */

function Select(parent, top, left, width, lenderer, hasDefault) {
  var s = document.createElement('select');

  s.style.position = "absolute";
  s.style.top   = String(parseInt(top))   + 'px';
  s.style.left  = String(parseInt(left))  + 'px';
  s.style.width = String(parseInt(width)) + 'px';

  parent.appendChild(s);

  this._element = s;
  this._lenderer = lenderer || SelectFunctor.DEFAULT;
  this._hasDefalut = hasDefault;

  this.enable(false);
};

S.extend(Select.prototype, Gadget);

// = public

Select.prototype.options = function (options) {
  if (options == null) {
    return;
  }

  this.clear();

  var count = 0;

  if (this._hasDefalut) {
    this._element.options[count] = new Option('-', '-1');
    count++;
  }

  for (var i = 0; i < options.length; i++) {
    var label = this._lenderer.label(options[i]);
    var value = this._lenderer.value(options[i]);
    this._element.options[count] = new Option(label, value);
    count++;
  }
};

Select.prototype.clear = function () {
  var length = this._element.options.length;
  for (var i = length; i >= 0; i--) {
    this._element.options[i] = null;
  }
};

Select.prototype.value = function (value) {
  var s = this._element;

  if (value == null) {
    return (s.selectedIndex >= 0) ?
           (s.options[s.selectedIndex].value) :
           (null);
  }

  if (value == -1) {
    s.selectedIndex = 0;

    return s.options[0].value;
  }

  for (var i = 0; i < s.options.length; i++) {
    if (s.options[i].value == value) {
      s.selectedIndex = i;
    }
  }

  return s.options[s.selectedIndex].value;
};

Select.prototype.setOnChange = function (f) {
    this._element.onchange = f;
};

Select.prototype.setOnFocus = function (f) {
    this._element.onFocus = f;
};

Select.prototype.setOnBlur = function (f) {
  this._element.onBlur = f;
};
