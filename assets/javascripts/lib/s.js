var S = {};

S.inherits = function(childCtor, parentCtor) {
  Object.setPrototypeOf(childCtor.prototype, parentCtor.prototype);
};

S.extend = function(destination, source) {
  for (property in source) {
    destination[property] = source[property];
  }
  return destination;
};
