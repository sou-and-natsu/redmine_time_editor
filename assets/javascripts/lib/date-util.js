var DateUtil = {};

DateUtil.dateString = function (date) {
  var y = date.getUTCFullYear();
  var m = date.getUTCMonth() + 1;
  var d = date.getUTCDate();

  var y0 = ('0000' + y).slice(-4);
  var m0 = ('00' + m).slice(-2);
  var d0 = ('00' + d).slice(-2);

  return y0 + '-' + m0 + '-' + d0;
};

DateUtil.prevSunDay = function (date) {
  var y = date.getUTCFullYear();
  var m = date.getUTCMonth();
  var d = date.getUTCDate() - date.getUTCDay();

  return new Date(y, m, d);
};

// Date
DateUtil.nextDate  = function (date, offset) {
  var offset = (offset != null) ? (offset) : (1);

  var new_date = new Date();
  var time = date.getTime();
  new_date.setTime(time + ((24 * 3600 * 1000) * offset));

  return new_date;
};

DateUtil.IsSameDate = function (lhs, rhs) {
  if (lhs.getFullYear() !== rhs.getFullYear()) { return false; }
  if (lhs.getMonth()    !== rhs.getMonth())    { return false; }
  if (lhs.getDate()     !== rhs.getDate())     { return false; }

  return true;
};
