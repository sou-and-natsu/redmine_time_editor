var ArrayUtil = {};

ArrayUtil.createFunctorBy = function (key) {

  var f = function (lhs, rhs) {
    var lhs_ = lhs[key];
    var rhs_ = rhs[key];

    if (lhs_ < rhs_) {
      return -1;
    } else if (lhs_ > rhs_){
      return 1;
    }

    return 0;
  };

  return f;
};
