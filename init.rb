Redmine::Plugin.register :redmine_time_editor do
  name 'Time Editor plugin'
  author 'Nauta'
  description 'Editor for your work time.'
  version '0.0.1'

  project_module :time_editor do
    permission :time_editor, :time_editor => :index
  end

  menu :project_menu,
       :time_editor,
       {
         :controller => 'time_editor',
         :action     => 'index'
       },
       {
         :caption => 'Time Editor',
         :param   => :project_id
       }
end
